## Isohedra

Isohedra is a 6-piece EP composed in [TidalCycles](https://tidalcycles.org) based on illustrations by artist [Caleb Ogg](ogg.haus). Listen to it on [spotify](https://open.spotify.com/album/4AQukgo5BCAsUE55WQ0daX), [soundcloud](https://soundcloud.com/ghalestrilo/sets/isohedra) or [audius](https://audius.co/ghalestrilo/album/isohedra-7342)

Or check the [post](https://ghales.top/art/isohedra) about it :)
